<?php

namespace App\Http\Controllers;

use SendGrid;
use App\Modelo;
use App\Opcional;
use SendGrid\Mail\Mail;
use Illuminate\Http\Request;
use Snowfire\Beautymail\Beautymail;

class ModeloController extends Controller
{
    public function show($id)
    {
        $opcionais = Modelo::opcionais($id);
        foreach ($opcionais as $opcional) {
            $acessorios = Opcional::whereIn('id', $opcionais)->pluck('descricao')->toArray();
        }
        // return $acessorios;
        $modelo = Modelo::where('id', '=', $id)->first();
        return view('layouts.modelo')->withModelo($modelo)->withAcessorios($acessorios);
    }

    public function mail(Request $request)
    {
        $email = new Mail();
        $email->setFrom($request->email, $request->nome);
        $email->setSubject("Intenção de compra seminovo");
        $email->addTo("seminovos@primorossi.com.br", "SEMINOVOS");
        $email->addCc("pcorreia@primorossi.com.br", "Patricia Correia");
        $email->addContent("text/plain", $request->email, $request->nome, $request->telefone);
        $email->addContent(
            "text/html",
            "<p>Cliente: <strong>" . $request->nome . "</strong></p>
            <p>Telefone: <strong>" . $request->telefone . "</strong></p>
            <p>E-mail: <strong>" . $request->email . "</strong></p>"
        );
        $sendgrid = new SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            // print $response->statusCode() . "\n";
            // print_r($response->headers());
            // print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ' . $e->getMessage() . "\n";
        }
        return view('obrigado');
    }
}
