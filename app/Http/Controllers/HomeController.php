<?php

namespace App\Http\Controllers;

use SendGrid;
use App\Banner;
use App\Cambio;
use App\Modelo;
use SendGrid\Mail\Mail;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::orderBy('ordem')->get();
        $modelos = Modelo::with('cambio')->paginate(8);

        return view('home', compact('banners', 'modelos'));
    }
    public function quem()
    {
        return view('layouts.quem');
    }
    public function contato()
    {
        return view('layouts.contato');
    }
    public function faleconosco(Request $request)
    {
        $email = new Mail();
        $email->setFrom($request->email, $request->nome);
        $email->setSubject("Intenção de compra seminovo");
        $email->addTo("seminovos@primorossi.com.br", "SEMINOVOS");
        $email->addCc("pcorreia@primorossi.com.br", "Patricia Correia");
        $email->addContent("text/plain", $request->email, $request->nome, $request->telefone);
        $email->addContent(
            "text/html",
            "<p>Cliente: <strong>" . $request->nome . "</strong></p>
            <p>Telefone: <strong>" . $request->telefone . "</strong></p>
            <p>E-mail: <strong>" . $request->email . "</strong></p>
            <p>Mensagem: <strong>" . $request->msg . "</strong></p>"
        );
        $sendgrid = new SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            // print $response->statusCode() . "\n";
            // print_r($response->headers());
            // print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
        return view('obrigado');
    }
}
