<?php

namespace App;

use App\Opcional;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
    public function acessorios()
    {
        return $this->hasMany(Opcional::class);
    }
    public function cambio()
    {
        return $this->belongsTo(Cambio::class);
    }

    public static function opcionais($id)
    {
        $opcionais = DB::table('modelo_opcional')
            ->where('modelo_id', '=', $id)
            ->pluck('opcional_id');
        return $opcionais;
    }
}
