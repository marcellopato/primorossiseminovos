<footer class="footer m-t-50">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Primo Rossi</h1>
                <p>Praça Pres. Kennedy, 134 - Móoca • São Paulo/SP • 03668-010 • Fone: (011) <a href="tel:4349-3435" class="text-white">4349-3435</a></p>
            </div>
            <div class="col">
                <img src="http://www.primorossiseminovos.com.br/storage/settings/February2019/73F7PoxU5CbHRRJYBAl2.png" alt="logo Primo Rossi" class="float-right" style="width:100px;">
            </div>
        </div>
    </div>
</footer>
