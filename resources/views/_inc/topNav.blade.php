<nav class="navbar navbar-expand-md navbar-dark bg-primary">
    <a class="navbar-brand" href="{{ route('home') }}">Primo Rossi Seminovos</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
            <a class="nav-link {{ Nav::isRoute('home')}}" href="{{ route('home') }}">Home </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Nav::isRoute('quem')}}" href="{{ route('quem')}}">Quem Somos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="http://www.primorossirentacar.com.br/" target="_blank">Aluguel de Veículos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link{{ Nav::isRoute('contato')}}" href="{{ route('contato')}}">Fale Conosco</a>
            </li>
        </ul>
        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <li class="text-white">Ligue e compre: (11) 4349-3435</li>
        </ul>
    </div>
</nav>
<div class="divisor_laranja"></div>
