@extends('layouts.app')
@section('content')
<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        @foreach($banners as $banner)
        <div class="carousel-item @if($banner->ordem == 1) ? active : @endif">
            <img class="d-block w-100" src="{{ Voyager::image($banner->imagem) }}" alt="First slide">
        </div>
        @endforeach
    </div>
</div>
<div class="container m-t-50 fullscreen">
    <h1 class="m-t-5">Compre seu seminovo na Primo Rossi</h1>
    <h4 class="m-b-50">Seminovos revisados, com garantia de fábrica e pouco tempo de uso.</h4>
    <div class="row">
        @foreach($modelos as $modelo)
        <div class="col-sm-3">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ $modelo->nome }}</h5>
                </div>
                <div style=" min-height: 169px; max-height: 169px;">
                    <img class="card-img-top" src="{{ asset(Voyager::image($modelo->destaque)) }}">
                </div>
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><span class="float-right preco"><span class="badge">R$</span> {{ $modelo->preco}}</span></li>
                        <li class="list-group-item">Placa final:<span class="badge badge-orange float-right m-t-5">{{ $modelo->placa}}</span></li>
                        <li class="list-group-item">Kilometragem:<span class="badge badge-orange float-right m-t-5">{{ $modelo->kilometragem}}</span></li>
                        <li class="list-group-item">Câmbio:<span class="badge badge-orange float-right m-t-5">{{ $modelo->cambio->descricao }}</span></li>
                        <li class="list-group-item">Combustível:<span class="badge badge-orange float-right m-t-5">{{ $modelo->combustivel}}</span></li>
                    </ul>
                </div>
                <div class="card-footer text-muted">
                    <a href="{{ route('modelo', $modelo->id) }}" class="btn btn-primary">Quero ver mais!</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    {{ $modelos->links() }}
</div>
@endsection
