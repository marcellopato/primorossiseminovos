@extends('layouts.app')
@section('content')
<div class="container">
    <div class="text-center m-t-100 m-b-200">
        <h1>Obrigado.</h1>
        <p>Em breve entraremos em contato com você!</p>
    </div>
</div>
@endsection()
