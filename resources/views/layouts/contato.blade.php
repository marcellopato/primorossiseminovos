@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row m-t-50">
            <div class="col">
                <img src="http://www.primorossiseminovos.com.br/storage/settings/February2019/epyL6yFUPF21d466ITaz.png" alt="logo Primo Rossi" class="float-left m-r-50 placa">
                <h1>Horário de funcionamento</h1>
                <h3>2ª a 6ª das 08hs às 18hs</h3>
                <br>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.4544079870393!2d-46.61052198502249!3d-23.552117984687435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce593c73a02d61%3A0x410ba27430b2a03f!2sPra%C3%A7a+Pres.+Kennedy%2C+134+-+Mooca%2C+S%C3%A3o+Paulo+-+SP%2C+03162-040!5e0!3m2!1spt-BR!2sbr!4v1548955601668"
                    width="600" height="450" frameborder="0" style="border:0" allowfullscreen class="mapa"></iframe>
            </div>
            <div class="col">
                <div class="divisor_laranja m-t-120 m-b-10"></div>
                <form action="{{ route('faleconosco') }}" enctype="application/x-www-form-urlencoded" method="POST">
                    @csrf @method('POST')
                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input type="text" class="form-control" name="nome" placeholder="Seu nome"> {{--
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Enter email">
                        <small id="emailHelp" class="form-text text-muted">Temos uma segura política de privacidade</small>
                    </div>
                    <div class="form-group">
                        <label for="msg">Deixe sua mensagem</label>
                        <textarea class="form-control" name="msg" id="msg" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="telefone">Telefone para contato</label>
                        <input type="text" class="form-control" name="telefone" v-mask="['(##) ####-####', '(##) #####-####']" placeholder="Somente números">
                        <small id="emailHelp" class="form-text text-muted">Fixo ou celular</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var app = new Vue({
        el: '#app',
        data: {
            //
        },
    });

</script>
@endsection
