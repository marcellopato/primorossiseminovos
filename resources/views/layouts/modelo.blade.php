@extends('layouts.app')
@section('content')
<div class="container m-t-50">
    <h4 class="m-b-50">Detalhes do modelo {{ $modelo->nome }}</h4>
    <div class="row">
        <div class="col-sm-6">
            <agile :arrows="false" :speed="750" :timing="'linear'" :fade="true" :autoplay="true" :pauseOnHover="false">
                @foreach ((json_decode($modelo->imagens)) as $foto)
                <div class="slide slide--1">
                    <img class="d-block w-100" src="{{ asset('storage/' . $foto) }}">
                    {{-- <img class="d-block w-100" src="{{ asset(str_replace(']','', str_replace('"','',stripslashes((str_replace('["','', 'storage/'. $foto)))))) }}"> --}}
                </div>
                @endforeach
            </agile>
            <hr>
            <h4>Opcionais</h4>
            <ul class="list-group">
                @foreach($acessorios as $acessorio)
                    <li class="list-group-item">{{ $acessorio }}</li>
                @endforeach
            </ul>

        </div>
        <div class="col-sm-6">
            <h1 class="preco">R$ {{ $modelo->preco }}</h1>
            <div class="divisor_laranja m-b-10"></div>
            <h3>Envie-nos sua intenção de compra!</h3>
            <form action="{{ route('intencaoCompra') }}" enctype="application/x-www-form-urlencoded" method="POST">
                @csrf
                @method('POST')
                <input type="hidden" name="modelo" value="{{ $modelo->nome }}">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" name="nome" placeholder="Seu nome"> {{--
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" placeholder="Enter email">
                    <small id="emailHelp" class="form-text text-muted">Temos uma segura política de privacidade</small>
                </div>
                <div class="form-group">
                    <label for="telefone">Telefone para contato</label>
                    <input type="text" class="form-control" name="telefone" v-mask="['(##) ####-####', '(##) #####-####']" placeholder="Somente números">
                    <small id="emailHelp" class="form-text text-muted">Fixo ou celular</small>
                </div>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    var app = new Vue({
        el: '#app',
        data: {
//
        },
    });
</script>
@endsection
