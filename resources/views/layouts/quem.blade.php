@extends('layouts.app')
@section('content')
    <div class="container">
        <h1 class="m-t-50">Nossa história sobre 4 rodas</h1>
        <hr>
        <p>Nossa história de sucesso é escrita dia a dia pelas mãos competentes da família Primo Rossi. É um talento que passa de geração
        para geração através do trabalho e do exemplo, motivando e treinando uma equipe capaz de conquistar resultados cada vez melhores.
        O filme que você vai assistir é uma prova viva desta energia. Uma mostra da força e do pioneirismo dos Primo Rossi, do que
        eles construíram e do que poderemos realizar juntos.</p>
        <div class="divisor_laranja m-t-40 m-b-50"></div>
        <div class="videoWrapper">
        <!-- Copy & Pasted from YouTube -->
        <iframe width="560" height="315" src="https://www.youtube.com/embed/fOnRYCHW3SE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
@endsection
@section('scripts')

