
require('./bootstrap');

window.Vue = require('vue');
import Vue from "vue";

Vue.component('detalhe-carro', require('./components/DetalheCarro.vue').default);

import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)

import VueAgile from "vue-agile";
Vue.use(VueAgile);

import Toasted from "vue-toasted";
Vue.use(Toasted);
