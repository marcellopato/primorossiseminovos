<?php


Route::get('/', 'HomeController@index')->name('home');
Route::get('quem', 'HomeController@quem')->name('quem');
Route::get('contato', 'HomeController@contato')->name('contato');
Route::post('faleconosco', 'HomeController@faleconosco')->name('faleconosco');
Route::get('modelo/{modelo}', 'ModeloController@show')->name('modelo');
Route::post('intencaoCompra', 'ModeloController@mail')->name('intencaoCompra');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
